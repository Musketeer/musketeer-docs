JavaScript API reference
========================

Before diving into the JavaScript API make sure you've signed-up and `installed Musketeer`_.

.. _installed Musketeer: /Installation.html

Musketeer('test', testName, variants, [options], [callback])
-----------------------------------------------

**Description**

Creates an A/B test and participates the user to the test. The user is automatically assigned to one
of the variants depending on the type of the test. See `Test types`_ for more information on the different
types of tests.

.. _Test types: /Test_types.html

When a user participates a test for the first time, the `participate` event is emitted with the test's
name and variant's name as arguments.

See `Musketeer('convert', testName)`_ on how to convert a user in a test.

============================	====================================================================================
Argument						Description
============================	====================================================================================
testName						The name of the test. The name should be between 1 and 32 characters and may only contain the characters a-z, A-Z, 0-9, - and _.
variants						An array of variants or variant names.

							In a test, at least two variants should exist and the variant names may only contain the characters a-z, A-Z, 0-9, - and _. If no variants are specified, two default variants are created: variant named A, and another variant named B. If the type of the test is ``random`` you can also specify a weight per variant. If you do not specify a weight, all variants will receive an equal weight.

							To provide a weight per variant:

							.. code-block:: javascript

								[{
									name: 'A',
									weight: 0.75,
								}, {
									name: 'B',
									weight: 0.25,
								}]

							And to create two variants with an equal weight:

							.. code-block:: javascript

								['A', 'B']

[options]						An optional options object for the test.

							=================	=====================
							Key					Description
							=================	=====================
							type				The type of test to create: ``random``, ``bandit``.
							=================	=====================
[callback]						The callback function which is invoked with the `variantName` and `testName` as arguments.
============================	====================================================================================

**Example**

.. code-block:: javascript
	:linenos:

	// Creates test `MyFirstTest` with variants A and B with no options.
	Musketeer('test', 'MyFirstTest', ['A', 'B'], { type: 'bandit' }, function (variantName) {
		if (variant === 'A') {
			// This is A.
		}
		else if (variant === 'B') {
			// And B.
		}
	});

Musketeer('convert', testName)
------------------------------

**Description**

Converts a user for the given test. Before converting a user make sure they're participating the test,
see `Musketeer('test', testName, variants, [options], [callback])`_.

After a user reaches the conversion goal, call this method to convert the user in the test.

When a user converts in a test, the `convert` event is emitted with the test's name and variant's name as arguments.

============================	====================================================================================
Argument						Description
============================	====================================================================================
testName						The name of the test in which the user converted.
============================	====================================================================================

**Example**

.. code-block:: javascript
	:linenos:

	// Convert the user for `MyFirstTest`.
	Musketeer('convert', 'MyFirstTest');

Musketeer('on', eventName, listener)
------------------------------------

**Description**

Adds the `listener` function to the listeners for the event named `eventName`. To remove a listener, use `Musketeer('off', eventName, [listener])`_.

============================	====================================================================================
Argument						Description
============================	====================================================================================
eventName						The name of the event.

							The following events are available.

							============	=========================================================================================================================================================================================
							Event			Description
							============	=========================================================================================================================================================================================
							load			This event is emitted after the Musketeer library finished loading.
							participate		This event is emitted when a user participates a test. ``listener`` is invoked with ``testName`` and ``variantName``.
										See `Musketeer('test', testName, variants, [options], [callback])`_.
							convert			This event is emitted when a user converts in a test. ``listener`` is invoked with ``testName`` and ``variantName``. See `Musketeer('convert', testName)`_.
							============	=========================================================================================================================================================================================
listener						The callback function.
============================	====================================================================================

**Example**

.. code-block:: javascript
	:linenos:

	Musketeer('on', 'participate', function (testName, variantName) {
		//
	});

Musketeer('off', eventName, [listener])
------------------------------------

**Description**

Removes the `listener` function or all listeners from the event `eventName`.

============================	====================================================================================
Argument						Description
============================	====================================================================================
eventName						The name of the event.
[listener]						The listener function to remove. If not specified, all listener functions will be removed.
============================	====================================================================================

**Example**

.. code-block:: javascript
	:linenos:

	Musketeer('off', 'participate');

Musketeer('config', config)
---------------------------

**Description**

Sets the config values of Musketeer.

====================		============================================================================================================================================================================================================================================================
Argument					Description
====================		============================================================================================================================================================================================================================================================
config				The config to set.

					====================	===================================================================================================================================================================================================================
					Config					Description
					====================	===================================================================================================================================================================================================================
					defaultTestType			The default test type to use when no type has been specified. The default is ``random``.
					participateTimeout		The participate timeout in miliseconds. The default is ``1000`` ms. If participating a test fails within this time, Musketeer will fallback to the ``random`` test type which doesn't require a network connection.
					====================	===================================================================================================================================================================================================================
====================		============================================================================================================================================================================================================================================================

**Example**

.. code-block:: javascript
	:linenos:

	Musketeer('config', {
		defaultTestType: 'random',
		participateTimeout: 1000,
	});

Musketeer('identify', userID)
-----------------------------

**Description**

Sets the user's unique identifier, for example, your user's id from your datastore. It's recommended
that you call identify once the user signs into your app or website.

Using identify helps Musketeer to serve the same variant to the same user on different devices or
browsers.

=============			===============================
Argument				Description
=============			===============================
userID					The unique user id of the user, either a string or a number.
=============			===============================

**Example**

.. code-block:: javascript
	:linenos:

	Musketeer('identify', user.id);

Musketeer('set', properties)
----------------------------

**Description**

Sets specific properties to the current user.

These properties can be used in filtering and analysing. For example, you can set the user's gender so
you can analyse the behaviour of your tests per gender.

=============			===============================
Argument				Description
=============			===============================
properties				A key-value object of user properties.
=============			===============================

**Example**

.. code-block:: javascript
	:linenos:

	Musketeer('set', {
		gender: 'Male',
		age: 18,
	});

Musketeer('check')
------------------

**Description**

Checks the current DOM for HTML-based tests. By default, check is triggered automatically. Only use
this method if you know what you are doing.

In HTML- and CSS-based tests, Musketeer needs to check the DOM for attributes which require actions.
Musketeer checks changes at various moments, like location changes, but sometimes it's required to
manually let Musketeer know to re-check the DOM, like when your client-side controllers finished
loading or your components finished rendering.

See `HTML-based tests`_ for more information on HTML-based tests.

.. _HTML-based tests: /HTML-based_tests.html

**Example**

.. code-block:: javascript
	:linenos:

	Musketeer('check');
