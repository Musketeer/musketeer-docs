Welcome to the Musketeer documentation!
=======================================

Musketeer is an A/B testing platform for developers and designers. If you have any questions, do not
hesitate to reach out to us at `support@musketeer.ai`_.

.. _support@musketeer.ai: mailto:support@musketeer.ai

.. toctree::
    :caption: Getting started

    What_is_Musketeer
    Installation
    Creating_your_first_test
    HTML-based_tests
    Using_CSS_selectors

.. toctree::
    :caption: Integration

    JavaScript_API_reference
    HTML_attributes_reference
    HTTP_API_reference

.. toctree::
    :caption: Advanced

    Test_types
