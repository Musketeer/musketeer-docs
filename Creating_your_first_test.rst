Tutorial: Creating your first test
========================

When you want to participate a user to a test, you can call `Musketeer('test', testName, variants, [options], [callback])`_.
This creates a test and participates the user to the test.

	.. _Musketeer('test', testName, variants, [options], [callback]): /JavaScript_API_reference.html#musketeer-test-testname-variants-options-callback

.. code-block:: javascript
	:linenos:

	Musketeer('test', 'MyFirstTest', ['A', 'B'], function (variant) {
		if (variant === 'A') {
			// The user is participating variant A.
		}
		else if (variant === 'B') {
			// The user is participating variant B.
		}
	});

Now, in a test you want the user to convert, sign up for a newsletter or perform a checkout. Once this
happens, you need to call `Musketeer('convert', testName)`_ to let Musketeer know the user converted.

.. _Musketeer('convert', testName): /JavaScript_API_reference.html#musketeer-convert-testname

.. code-block:: javascript
	:linenos:

	Musketeer('convert', 'MyFirstTest');

Please have a look at the `JavaScript API reference`_ to read more about the complete JavaScript API.

.. _JavaScript API reference: /JavaScript_API_reference.html
