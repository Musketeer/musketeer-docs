HTML attributes reference
=========================

musketeer-test=testName
-----------------------------

**Description**

This sets the name of the test to `testName`.

Whenever this attribute is present in the DOM, Musketeer creates the given test and participates the
user automatically. Please note that Musketeer ignores the visiblity of the element. For example, if
you've set it to `display: none` the test will still be created.

You can create variants for the test with `musketeer-variant=variantName`_.

A test name should be between 1 and 32 characters long and may not contain ``.``, ``,``, and ``+`` characters.

When your site's HTML finishes loading, Musketeer checks your HTML to see if there are any Musketeer
attributes. If there are, Musketeer converts these attributes to actions. You can invoke this check
manually by calling `Musketeer('check')`_.

.. _Musketeer('check'): /JavaScript_API_reference.html#musketeer-check

**Example**

.. code-block:: html

	<div musketeer-test="MyFirstTest">
		<h1 musketeer-variant="A">Sign up</h1>
		<h1 musketeer-variant="B">Sign up now, it's free.</h1>
	</div>

musketeer-variant=variantName
-----------------------------

**Description**

This defines a variant of a test. It's important to set this attribute on a child node
of `musketeer-test=testName`_.

When a user participates a test and joins a variant, all other variants in the DOM of the test will
be hidden. The variant the user joined will receive a ``musketeer-active`` class and will become
visible.

A variant name should not contain `+` or `,` characters.

**Example**

``musketeer-variant`` attribute should always be declared on a child element of `musketeer-test=testName`_.

.. code-block:: html

	<div musketeer-test="MyFirstTest">
		<h1 musketeer-variant="A">Sign up</h1>
		<h1 musketeer-variant="B">Sign up now, it's free.</h1>
	</div>

musketeer-type=testType
-----------------------------

Sets the type of the given test. It's important to set this attribute on the same element as `musketeer-test=testName`_.

`testType` can be one of the following strings: ``random`` (default) or ``bandit``.
Please have a look at `Test types`_ to learn more about the different test types.

.. _Test types: /Test_types.html

**Example**

Creates a test of type ``bandit``.

.. code-block:: html

	<div musketeer-test="MyFirstTest" musketeer-type="bandit">
		<h1 musketeer-variant="A">Sign up</h1>
		<h1 musketeer-variant="B">Sign up now, it's free.</h1>
	</div>

musketeer-weight=variantWeight
-----------------------------

**Description**

Sets the weight of a variant. It's imporant to set this attribute on the same element as `musketeer-variant=variantName`_.

The weight should be a number between 0 and 1 where 0 means 0% of the participants will join the variant and 1 means 100% of the participants will join the variant.

Please note that you cannot set weights of variants in a multi-armed bandit test.

**Example**

Sets the weight of variant A to 75% and the weight of variant B to 25%.

.. code-block:: html

	<div musketeer-test="MyFirstTest">
		<h1 musketeer-variant="A" musketeer-weight="0.75">Sign up</h1>
		<h1 musketeer-variant="B" musketeer-weight="0.25">Sign up now, it's free.</h1>
	</div>

musketeer-convert=testName
-----------------------------

**Description**

Converts the user in the test.

Whenever this attribute exists in the DOM, Musketeer converts the user in the test with the specified
name. It's important to understand that Musketeer is not looking at e.g. `display: none` of the target
element, but rather it's existance in the DOM.

**Example**

.. code-block:: html

	<body musketeer-convert="MyFirstTest">
		<h1>Thank you for signing up.</h1>
	</body>

musketeer-identify=userID
-----------------------------

**Description**

Identifies the user, see `Musketeer('identify', userID)`_.

.. _Musketeer('identify', userID): /JavaScript_API_reference.html#musketeer-identify-userid

**Example**

.. code-block:: html

	<body musketeer-convert="MyFirstTest">
		<h1>Thank you for signing up.</h1>
	</body>

musketeer-property-:propertyName=value
-----------------------------

**Description**

This sets a property with name ``:propertyName`` and value ``value`` to the current user. This allows
you to set custom properties on users which you can use to segment users.

This attribute must be set on the same element as `musketeer-test=testName`_

**Example**

Sets the gender of the current user to female.

.. code-block:: html

	<div musketeer-test="MyFirstTest" musketeer-property-gender="female">
		<h1 musketeer-variant="A">Sign up</h1>
		<h1 musketeer-variant="B">Sign up now, it's free.</h1>
	</div>
