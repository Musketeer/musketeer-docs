Event source
============

.. note:: Event source is an experimental feature and is disabled by default. You can enable it in the config. Please use with caution.

When a user participates a test, we need to make a round-trip to the server which calculates the target
variant. This should often be fairly fast but we're experimenting whether we can eliminate this trip
completely.

**What is event source?**

Event source is a persistent connection to Musketeer's servers. It sends the target variant of all
tests the user is not participating yet. When the user participates a test, the target variant is already
known and avoid a round-trip.

The downside is that the target variant might chance while the user receive a variant and before the
user participates a test. Especially with a lot of visitors, this might results in a 
