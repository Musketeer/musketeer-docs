Tutorial: CSS selectors
=====================

When a user participates a test, Musketeer adds a class to the body so you can style elements directly
in CSS without having to do much more.

To format of a class is `musketeer-testName-variantName` where testName is the name of the test and
variant name the name of the variant.

Because you're allowed to use spaces in test names and variants, spaces get replaced by dashes.

**Example**

We create a test named ``MyTest`` with variants A and B.

.. code-block:: javascript

	Musketeer('test', 'MyTest', ['A', 'B']);

To style the button for in variant B, we can create a selector like the below with ``body.musketeer-MyTest-B``
followed by the target element, see below.

.. code-block:: css

	.btn {
		background: gray;
		color: black;
	}
	body.musketeer-MyTest-B .btn {
		background: red;
		color: white;
	}

In SCSS, you can easily style the button based on the class of the body:

.. code-block:: scss

	.btn {
		background: gray;
		color: black;

		body.musketeer-MyTest-B & {
			background: red;
			color: white;
		}
	}
