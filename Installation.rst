Installation
============

Before you can get started, you need to sign up for Musketeer at https://musketeer.ai/. Once you signed up,
you are given a unique snippet you should place in your `<head>` of your site.
