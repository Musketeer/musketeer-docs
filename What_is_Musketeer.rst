What is Musketeer?
===============

Musketeer is an A/B test platform for developers, designers and growth hackers. It helps you to create
several different types of tests: random and bandit tests. These different types helps you
to test variants differently. See `Test types`_ for more information on the different types of tests.

.. _Test types: /Test_types.html

In Musketeer, you write tests in `JavaScript`_ or in `HTML`_. You can also use `CSS`_ directly to style elements
per variant.

.. _JavaScript: /JavaScript_API_reference.html
.. _HTML: /HTML_attributes_reference.html
.. _CSS: /Using_CSS_selectors.html

In case you want to leverage Musketeer in your back-end, we've exposed our `HTTP API`_.

.. _HTTP API: /HTTP_API_reference.html

Instead of a traditional platform where you need to create tests in a WYSIWYG editor, in Musketeer, you
create tests where you are most comfortable: your code.

This has several advantages:

- You do not have to work in a WYSIWYG editor, but you can use your own IDE where you are most comfortable.
- Your tests are stored in your revision control system. This means you can easily manage development, staging and production environments, and you can more easily review and collaborate on tests.
- The code of your variants are not stored at an external party. Instead, it's directly served in your site which could be faster.
- The external library becomes much smaller. Musketeer's library is easily 5 times smaller than traditional platforms.

Please continue to the `Installation`_ page to get started installing Musketeer. If you have any questions,
do not hesitate to reach out to us at `support@musketeer.ai`_.

.. _support@musketeer.ai: mailto:support@musketeer.ai
.. _Installation: /Installation.html
