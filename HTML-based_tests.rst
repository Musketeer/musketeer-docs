Tutorial: HTML-based tests
======================

Instead of using JavaScript, sometimes it's easier to just use HTML directly. With Musketeer you can.

Let's create a different header depending on the user's variant. In the control variant, we're showing
a "Sign up now." header and in our new variant we'll show "Sign up now. It's free.".

.. code-block:: html
	:linenos:

	<div musketeer-test="MyFirstTest">
		<h1 musketeer-variant="A">
			Sign up now.
		</h1>

		<h1 musketeer-variant="B">
	 		Sign up now. It's free.
		</h1>

		<a class="btn" href="/sign-up">Sign up</a>
	</div>

If you want, or if you're required, you may prefix the attributes with `data-`.

On line 9 in the above example you can see the link to the `/sign-up` page. In this page, we simply
add the musketeer-convert attribute, like illustrated below.

.. code-block:: html
	:linenos:

	<body musketeer-convert="MyFirstTest">
		<form>
			<!--
				Sign up form here
			-->
		</form>
	</body>

When your site's HTML finishes loading, Musketeer checks your HTML to see if there are any Musketeer
attributes. If there are, Musketeer converts these attributes to actions. You can invoke this check
manually by calling `Musketeer('check')`_.

.. _Musketeer('check'): /JavaScript_API_reference.html#musketeer-check

Please have a look at the `HTML attributes reference`_ to read more about the HTML attributes.

.. _HTML attributes reference: /HTML_attributes_reference.html
