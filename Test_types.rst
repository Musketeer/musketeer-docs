Test types
==========

You can create different types of tests. Every type is backed by a different algorithm which distributes
the participants of the tests differently among the variants.

random
------
The ``random`` test distributes participants randomly in a variant. This type does not use a network
request. This type is also used as a fallback if the other types, for examples the bandit type, fails,
see `config.participateTimeout`_.

.. _config.participateTimeout: /JavaScript_API_reference.html#musketeer-config-config

bandit
---------------------------
The ``bandit`` test is an advanced test which tries to find the most profitable variant in your test
and changes the participants distribution accordingly.

In a bandit test, a couple of times per day, Musketeer looks at the performance of every variant and slightly
adjusts the traffic of the variants. A variant with a high conversion rate will receive more traffic
and a variant with lower conversion rates will receive less traffic.
